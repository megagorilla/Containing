package nhl.containing.client.entities;

import com.jme3.scene.Node;

/**
 * 
 * @author Sander
 * All other platforms extend from this platform class, that is why we keep this empty class in our project
 */
public abstract class Platform extends Node
{

	public Platform()
	{
	}

}
