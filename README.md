Containing
==========

Percentages:
------------
* Vrachtauto’s 47%
* Treinen 8%
* Zeevaart 20%
* Binnenvaart 25%

AGV (Automatic Guided Vehicle):
-------------------------------
* Zelfstandig alles kunnen doen
* Communicatie met het centrale systeem
* 100 instanties
* Opdracht punt A naar B met de kortste route

Kranen:
-------
* Vrij beweegbare kranen
* Railkranen
* 10 zeeschipkranen
* 8 binnenvaartkranen
* 4 treinkranen
* 20 vrachtautokranen

Platformen:
-----------

###Treinplatform
* 1 Trein
* 1 of meerdere treinkranen

###Vrachtwagenplatform
* Meerdere vrachtwagens
* Rijdende kraan

###Binnenvaartplatform
* Meerdere binnenvaartschepen
* Meerdere binnenvaartkranen
* Kan evenwijdig aan de kade verplaatsen ( via een rail )

###Zeeschepenplatform
* 1 Zeeschip
* Meerdere rijdende kranen
* AGV op afgesplitste weg

Opslagterrein:
--------------
* 6 container stapels in de lengte
* Maximaal 6 containers op elkaar
* 6 stroken voor containers
* Parkeerplaats beide kanten voor 6 AGV’s
* Opslagterrein is 1550 meter lang en 600 meter breed
* Elke strook heeft 1 opslagrailkraan

Weg:
----
* 2 of 4 banen

Containers:
-----------
* Lengte = 44ft (rest nog even opzoeken) (13,555 mm* 2,348 mm * 2,695 mm)
* RFID tags vol met gegevens, koppeling aan de container
* Containers mits mogelijk op tijd sorteren in opslag

#container count (XML7)
##aankomst
* zeeschip: 18904 containers
* trein: 1570 containers
* binnenschip: 5361 containers
* vrachtauto: 25835 containers

##vertrek
* vrachtauto: 15339 containers
* binnenschip: 18484 containers
* trein: 6326 containers
* zeeschip: 11521 containers
